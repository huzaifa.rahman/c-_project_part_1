#include <iostream>
#include "student.h"

/*
CORNER __CASES
    if subject doesnt exit then marks are returned as -1
    if marks are already set then it is overwritten
*/

using namespace std;

int main()
{
    // initialization of objects
    student student1("12345", 20, 3.5);
    student student2("54321", 22, 3.7);

    // filling some data
    student1.set_subject_marks("Eng", 80);
    student1.set_subject_marks("Urdu", 90);
    student2.set_subject_marks("P.Std", 80);
    student2.set_subject_marks("Math", 90);

    // fetching data using member functions
    cout << "\n=== Student 1 data ===\n"
         << "roll number : " << student1.getRollNo() << endl
         << "Age : " << student1.getAge() << endl
         << "CGPA : " << student1.getCGPA() << endl
         << "English marks : " << (student1.get_subject_marks("Eng") == -1 ? -1 : student1.get_subject_marks("Eng")) << endl
         << "=== Student 1 all subjects marks ===\n";
    student1.print_all_marks();
    cout << "=========================\n\n";

    cout << "=== Student 2 data ===\n"
         << "roll number : " << student2.getRollNo() << endl
         << "Age : " << student2.getAge() << endl
         << "CGPA : " << student2.getCGPA() << endl
         << "Maths marks : " << (student2.get_subject_marks("Math") == -1 ? -1 : student2.get_subject_marks("Math")) << endl
         << "=== Student 2 all subjects marks ===\n";
    student2.print_all_marks();
    cout << "=========================\n\n";
    return 0;
}