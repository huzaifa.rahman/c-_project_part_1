#include <string>
#include <map>

using namespace std;

class student
{
private:
  struct student_record
  {
    string roll_no;
    int age;
    float cgpa;
  } record;
  map<string /*subject name*/, int /*marks*/> result;

public:
  student(string roll_no, int age, float cgpa);
  ~student();
  int get_subject_marks(string subject);
  void set_subject_marks(string subject, int marks);
  void print_all_marks();
  string getRollNo();
  void setRollNo(string roll_no);
  int getAge();
  void setAge(int age);
  float getCGPA();
  void setCGPA(float cgpa);
};