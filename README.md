# C++_project_part_1

This project is about understanding basic C++ class concepts e.g how a class is properly implemented and used in a C++ program.

## Separating Implementaion and definition

I have done this by including `student.h` file in `student.cpp` file in `src` directory and defining the member functions there.

## Building a static library 

This is done by `add_library` function in `CMakeLists.txt` file of `src` directory and also including the `include` path of `include` directory for header files. In `app` directory `CMakeLists.txt` library is linked to the target using `target_link_libraries`.

## Writing Constructor and Destructor 

Constructor is written to initialize the class data. Both constructors and destructors include simple print statments to show their execution.
