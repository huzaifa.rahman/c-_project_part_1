#include "student.h"
#include <iostream>

using namespace std;

student::student(string roll_no, int age, float cgpa)
{
  std::cout << "Constructor is called with Roll no = " << roll_no
            << ", age = " << age
            << " and CGPA = " << cgpa << std::endl;
  student::setRollNo(roll_no);
  student::setAge(age);
  student::setCGPA(cgpa);
}
student::~student()
{
  std::cout << "Destructor is called" << std::endl;
}
int student::get_subject_marks(string subject)
{
  auto search = result.find(subject);
  if (search != result.end())
  {
    return search->second;
  }
  else
  {
    return -1; // -1 because valid marks can not be negative
  }
}
void student::set_subject_marks(string subject, int marks)
{
  auto search = result.find(subject);
  if (search != result.end())
  {
    result.erase(search);
    result.insert({subject, marks});
  }
  else
  {
    result.insert({subject, marks});
  }
}
void student::print_all_marks()
{
  for (auto const &x : result)
  {
    std::cout << x.first // string (key)
              << ':'
              << x.second // string's value
              << std::endl;
  }
}
string student::getRollNo()
{
  return record.roll_no;
}
void student::setRollNo(string roll_no)
{
  record.roll_no = roll_no;
}
int student::getAge()
{
  return record.age;
}
void student::setAge(int age)
{
  record.age = age;
}
float student::getCGPA()
{
  return record.cgpa;
}
void student::setCGPA(float cgpa)
{
  record.cgpa = cgpa;
}